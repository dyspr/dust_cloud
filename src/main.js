var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var parameters = []
var parameter = 0.6589
var size = 21
var dustSize = 0.025
var rotation = 0.001

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  translate(windowWidth * 0.5, windowHeight * 0.5)
  rotate(frameCount * rotation)
  for (var i = 0; i < size; i++) {
    for (var j = 0; j < size; j++) {
      rotate(Math.PI * parameter)
      push()
      translate(0 * windowWidth * 0.5 + (i - 25) * boardSize * 0.01, 0 * windowHeight * 0.5 + (j - 25) * boardSize * 0.015)
      noStroke()
      fill(255 - j * (255 / (size * 2)))
      ellipse(0, 0, boardSize * dustSize * (2 * sin(frameCount * 0.1 + j * Math.PI * 0.01 * (i + 1))))
      pop()
    }
  }
  rotation = 0.001 + 0.999 * 0.05 * ((mouseX - windowWidth * 0.5) / windowWidth)
  dustSize = 0.001 + 0.0125 * (mouseY / windowHeight)
}

function mousePressed() {
  parameter = Math.random() * 2
  size = 7 + 2 * Math.floor(Math.random() * 21) + 1
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
